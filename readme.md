# Galleryv2 2022

GalleryV22022 est la version évolué du projet gallery

## Installation

Installer [scoop](https://scoop.sh/) pour pouvoir installer Symfony.

Installer Symfony
```bash
scoop install symfony-cli
```

Installer [composer](https://getcomposer.org/download/)

Installer un serveur Web local [Wamp](https://www.wampserver.com/)

## Configuration du projet
Dans le terminal de votre IDE

```bash
composer update
```
Si la base de donnée existe déjà
```bash
php bin/console doctrine:database:drop --force
```
Ensuite
```bash
php bin/console doctrine:database:create
```
```bash
php bin/console make:migration
```
```bash
php bin/console --no-interaction doctrine:migration:migrate
```
```bash
php bin/console --no-interaction doctrine:fixture:load
```


## Utilisation

### Démarrage

Veuillez ouvrir WAMP/MAMP/XAMPP pour la base de donnée

Démarrer le serveur
```bash
symfony server:start
```

### Navigation

* Explorer le [site](https://127.0.0.1:8000)
* Créer un [compte](https://127.0.0.1:8000/registration)
* Se [connecter](https://127.0.0.1:8000/login)

### Compte administrateur :

* login: ``pat.mar@gmail.com``
* password: ``password``
* Se connecter en [`Administrateur`](https://127.0.0.1:8000/login?email=pat.mar@gmail.com&pwd=password)


* Panel [Administration](https://127.0.0.1:8000/admin/painting)

## Controller

#### Admin
* [`Peinture`](./src/Controller/Admin/AdminPaintingController.php)
* [`Commentaire`](./src/Controller/Admin/AdminCommentController.php)
* [`Commande`](./src/Controller/Admin/AdminOrderController.php)
* [`Slider`](./src/Controller/Admin/AdminSliderController.php)
* [`Utilisateur`](./src/Controller/Admin/AdminUserController.php)
* [`Messagerie`](./src/Controller/Admin/AdminMessageController.php)

### User
* [`Home`](./src/Controller/HomeController.php)
* [`Profile`](./src/Controller/ProfileController.php)
* [`Panier`](./src/Controller/CartController.php)
* [`Detail d'un tableau`](./src/Controller/DetailController.php)
* [`Messagerie`](./src/Controller/MessagerieController.php)
* [`Commande`](./src/Controller/OrderController.php)
* [`Slider`](./src/Controller/SliderController.php)
* [`Registration`](./src/Controller/RegistrationController.php)
* [`Login`](./src/Controller/LoginController.php)
* [`Messagerie`](./src/Controller/MessagerieController.php)

## Vue

### Admin
* [`Peinture`](./templates/admin/painting/painting.html.twig)
* [`Commentaire`](./templates/admin/comment/comment.html.twig)
* [`Commande`](./templates/admin/order/orders.html.twig)
* [`Slider`](./templates/admin/slider/slider.html.twig)
* [`Utilisateur`](./templates/admin/users/users.html.twig)

### User
* [`Home`](./templates/home/home.html.twig)
* [`Profile`](./templates/profile/profile.html.twig)
* [`Panier`](./templates/cart/cart.html.twig)
* [`Detail d'un tableau`](./templates/detail/detail.html.twig)
* [`Messagerie`](./templates/messagerie/index.html.twig)
* [`Commande`](./templates/order/index.html.twig)
* [`Slider`](./templates/partials/slider.html.twig)
* [`Registration`](./templates/register/register.html.twig)
* [`Login`](./templates/login/login.html.twig)
* [`Messagerie`](./templates/messagerie/index.html.twig)

## Contribution

Ce projet est un travail à but d'être évalué donc il est privé


## License

Nul