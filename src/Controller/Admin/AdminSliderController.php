<?php

namespace App\Controller\Admin;

use App\Entity\Slider;
use App\Form\Admin\SliderType;
use App\Repository\SliderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminSliderController extends AbstractController
{
    /**
     * @param SliderRepository $sliderRepository
     * @return Response
     */

    #[Route('/admin/slider', name: 'app_admin_slider')]
    #[IsGranted('ROLE_SUPER_ADMIN')]
    public function slider(SliderRepository $sliderRepository): Response
    {
       $sliders = $sliderRepository->findAll();

       return $this->render('admin/slider/slider.html.twig', [
          'sliders' => $sliders
       ]);
    }


   /**
    * @param EntityManagerInterface $manager
    * @param slider $slider
    * @return Response
    */

   #[Route('/admin/slider/view/{id}', name: 'app_admin_slider_view')]
   #[IsGranted('ROLE_SUPER_ADMIN')]
   public function sliderView(EntityManagerInterface $manager, slider $slider):Response
   {
       $slider->setIsVisible(!$slider->isIsVisible());
       $manager->flush();
       if ($slider->isIsVisible()){
           $label = 'success';
           $sneak = 'affiché';
       } else {
           $label = 'info';
           $sneak = 'caché';
       }
       $this->addFlash($label, 'Le slider '.$slider->getId().' a bien été '.$sneak);
      return $this->redirectToRoute('app_admin_slider');
   }

   /**
    * @param slider $slider
    * @param EntityManagerInterface $manager
    * @return Response
    */

   #[Route('/admin/slider/delete/{id}', 'app_admin_slider_delete')]
   #[IsGranted('ROLE_SUPER_ADMIN')]
   public function sliderDelete(Slider $slider, EntityManagerInterface $manager):Response
   {
      $id = $slider->getId();
      $manager->remove($slider);
      $manager->flush();
      $this->addFlash('success', 'Le slider '.$id.' a bien été supprimé');
      return $this->redirectToRoute('app_admin_slider');
   }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */

   #[Route('/admin/slider/new', name: 'app_admin_slider_new')]
   #[IsGranted('ROLE_SUPER_ADMIN')]
   public function sliderNew(Request $request, EntityManagerInterface $manager):Response
   {
      $slider = new Slider();
      $form = $this->createForm(SliderType::class, $slider);
      $form->handleRequest($request);
      if($form->isSubmitted() && $form->isValid()){
         $slider->setIsVisible(true)
               ->setUpdatedAt(new \DateTimeImmutable());
         $manager->persist($slider);
         $manager->flush();
          $this->addFlash('success', 'Le slider '.$slider->getId().' a bien été crée');
         return $this->redirectToRoute('app_admin_slider');
      }

      return $this->renderForm('admin/slider/sliderNew.html.twig', [
         'form' => $form,
      ]);
   }
}
