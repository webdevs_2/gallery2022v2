<?php

namespace App\Controller\Admin;

use App\Entity\Message;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminMessageController extends AbstractController
{
    /**
     * @param MessageRepository $messageRepository
     * @return Response
     */
    #[Route('/admin/message', name: 'app_admin_message')]
    public function index(MessageRepository $messageRepository): Response
    {
        $messages = $messageRepository->findAll();
        return $this->render('admin/messagerie/index.html.twig', [
            'messages' => $messages,
        ]);
    }

    /**
     * @param Message $message
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/admin/message/read/{id}', name: 'app_admin_message_read')]
    public function read(Message $message, EntityManagerInterface $manager): Response
    {

        return $this->render('admin/messagerie/detail.html.twig', compact("message"));
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param UserRepository $userRepository
     * @return Response
     */
    #[Route('/admin/message/send', name: 'app_admin_message_send')]
    public function send(Request $request, EntityManagerInterface $manager, UserRepository $userRepository): Response
    {
        $message = new Message();

        $admin = $userRepository->findBy(
            ['email' => 'admin.gallery@gallery.com']
        );

        $form = $this->createForm(MessageType::class, $message);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $message->setSender($admin[0]);
            $manager->persist($message);
            $manager->flush();

            $this->addFlash('success', 'Le message a bien été envoyé avec succès.');
            return $this->redirectToRoute('app_admin_message');
        }

        return $this->render('admin/messagerie/send.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Message $message
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/admin/message/delete/{id}', name: 'app_admin_message_delete')]
    public function delete(Message $message,EntityManagerInterface $manager): Response
    {
        $manager->remove($message);

        $manager->flush();
        $this->addFlash('success', 'Le message a bien été supprimé');
        return $this->redirectToRoute("app_admin_message");
    }
}
