<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\Admin\RegistrationAdminType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class AdminUserController extends AbstractController
{
    /**
     * @param UserRepository $repository
     * @return Response
     */

    #[Route('/admin/user', name: 'app_admin_user')]
    #[IsGranted('ROLE_SUPER_ADMIN')]
    public function index(UserRepository $repository): Response
    {
        $users = $repository->findBy(
            [],
            ['username' => 'DESC']
        );
        return $this->renderForm('admin/users/users.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @param User $user
     * @param EntityManagerInterface $manager
     * @param $role
     * @return Response
     */

    #[Route('/admin/user/promote/{id}/{role}', name: 'app_admin_user_promote')]
    #[IsGranted('ROLE_SUPER_ADMIN')]
    public function promote(User $user, EntityManagerInterface $manager, $role): Response
    {
        $user->setRoles([$role]);
        $manager->flush();
        $this->addFlash('success', $user->getUsername() . ' a bien été promu');
        return $this->redirectToRoute('app_admin_user');
    }

    /**
     * @param EntityManagerInterface $manager
     * @param User $user
     * @return Response
     */

    #[Route('/admin/user/view/{id}', name: 'app_admin_user_view')]
    #[IsGranted('ROLE_SUPER_ADMIN')]
    public function view(EntityManagerInterface $manager, User $user): Response
    {
        $user->setIsDisabled(!$user->isIsDisabled());
        $manager->flush();
        if ($user->isIsDisabled()) {
            $label = 'info';
            $sneak = 'désactivé';
        } else {
            $label = 'success';
            $sneak = 'activé';
        }
        $this->addFlash($label, 'L\'utilisateur "' . $user->getUsername() . '" a bien été ' . $sneak);

        return $this->redirectToRoute('app_admin_user');
    }

    /**
     * @param Request $request
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param EntityManagerInterface $manager
     * @return Response
     */

    #[Route('/admin/user/new', name: 'app_admin_user_new')]
    #[IsGranted('ROLE_SUPER_ADMIN')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $manager): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationAdminType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if (empty($user->getImageFile())) {
                $user->setImageName('default.jpg');
            }
            $user->setPassword($userPasswordHasher->hashPassword($user, $form->get('pwd')->getData()))
                ->setCreatedAt(new \DateTimeImmutable())
                ->setUpdatedAt(new \DateTimeImmutable())
                ->setIsDisabled(false);
            switch ($form->get('role')->getData()) {
                case 'ROLE_ADMIN':
                    $user->setRoles(['ROLE_ADMIN']);
                    break;
                case 'ROLE_SUPER_ADMIN':
                    $user->setRoles(['ROLE_SUPER_ADMIN']);
                    break;
                default:
                    $user->setRoles(['ROLE_USER']);
                    break;
            }
            $manager->persist($user);
            $manager->flush();
            $this->addFlash('success', 'L\'utilisateur ' . $user->getUsername() . ' a bien été crée');
            return $this->redirectToRoute('app_admin_user');
        }

        return $this->render('admin/users/userNew.html.twig', [
            'registrationAdminForm' => $form->createView(),
        ]);
    }
}
