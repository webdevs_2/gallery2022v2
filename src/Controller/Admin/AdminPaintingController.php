<?php

namespace App\Controller\Admin;

use App\Entity\Painting;
use App\Form\Admin\PaintingType;
use App\Repository\PaintingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminPaintingController extends AbstractController
{
    /**
     * @param PaintingRepository $paintingRepository
     * @return Response
     */

   #[Route('/admin/painting', name: 'app_admin_painting')]
   #[IsGranted('ROLE_ADMIN')]
   public function painting(PaintingRepository $paintingRepository, ): Response
   {
      $paintings = $paintingRepository->findAll();
      return $this->render('admin/painting/painting.html.twig', [
         'paintings' => $paintings,
      ]);
   }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */

    #[Route('/admin/painting/new', name: 'app_admin_painting_new')]
    #[IsGranted('ROLE_ADMIN')]
    public function paintingNew(Request $request, EntityManagerInterface $manager):Response
    {
        $painting = new Painting;
        $form = $this->createForm(PaintingType::class, $painting);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $painting->setCreatedAt(new \DateTimeImmutable())
                     ->createSlug();
            $manager->persist($painting);
            $manager->flush();
            $this->addFlash('success', 'Le tableau "' . $painting->getTitle() . '" a été crée avec succès');
            return $this->redirectToRoute('app_admin_painting');
        }

        return $this->renderForm('admin/painting/paintingNew.html.twig', [
            'form' => $form,
        ]);
    }

   /**
    * @param Painting $painting
    * @param EntityManagerInterface $manager
    * @return Response
    */

   #[Route('/admin/painting/delete/{id}', 'app_admin_painting_delete')]
   #[IsGranted('ROLE_SUPER_ADMIN')]
   public function delete(Painting $painting, EntityManagerInterface $manager):Response
   {
      $manager->remove($painting);
      $manager->flush();
       $this->addFlash('success','Le tableau "'.$painting->getTitle().'" a bien été supprimé');
      return $this->redirectToRoute('app_admin_painting');
   }

   /**
    * @param Request $request
    * @param Painting $painting
    * @param EntityManagerInterface $manager
    * @return Response
    */

   #[Route('/admin/painting/edit/{id}', name: 'app_admin_painting_edit')]
   #[IsGranted('ROLE_SUPER_ADMIN')]
   public function edit(Request $request, Painting $painting, EntityManagerInterface $manager) :Response
   {
      $form = $this->createForm(PaintingType::class, $painting);
      $form->handleRequest($request);
      if($form->isSubmitted() && $form->isValid()) {
         $painting->createSlug();
         $manager->flush();
         $this->addFlash('success', 'Le tableau '.$painting->getTitle().' a bien été modifié');
         return $this->redirectToRoute('app_admin_painting');
      }
      return $this->renderForm('admin/painting/paintingEdit.html.twig', [
         'form' => $form,
      ]);
   }

   /**
    * @param EntityManagerInterface $manager
    * @param Painting $painting
    * @return Response
    */

   #[Route('/admin/painting/view/{id}', name: 'app_admin_painting_view')]
   #[IsGranted('ROLE_ADMIN')]
   public function view(EntityManagerInterface $manager, Painting $painting):Response
   {
      $painting->setIsPublished(!$painting->isIsPublished());
      $manager->flush();
       if ($painting->isIsPublished()){
           $label = 'success';
           $sneak = 'affiché';
       } else {
           $label = 'info';
           $sneak = 'caché';
       }
       $this->addFlash($label, 'Le tableau "'.$painting->getTitle().'" a bien été '.$sneak);
      return $this->redirectToRoute('app_admin_painting');
   }


}
