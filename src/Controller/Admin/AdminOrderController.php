<?php

namespace App\Controller\Admin;

use App\Entity\Invoice;
use App\Repository\InvoiceRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminOrderController extends AbstractController
{
    /**
     * @param InvoiceRepository $invoiceRepository
     * @return Response
     */
    #[Route('/admin/orders', name: 'app_admin_order')]
    #[IsGranted('ROLE_ADMIN')]
    public function orders(InvoiceRepository $invoiceRepository): Response
    {
        $orders = $invoiceRepository->findAll();
        return $this->render('admin/order/orders.html.twig', [
            'orders' => $orders,
        ]);
    }

    /**
     * @param Invoice $invoice
     * @return Response
     */
    #[Route('/admin/order/detail/{id}', name: 'app_admin_order_detail')]
    #[IsGranted('ROLE_ADMIN')]
    public function orderDetail(Invoice $invoice): Response
    {
        $total = 0;
        foreach ($invoice->getOrders() as $order) {
            $total += $order->getPainting()->getPrice();
        }

        return $this->render('admin/order/orderDetail.html.twig', [
            'orders' => $invoice,
            'total' => $total
        ]);
    }
}
