<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCommentController extends AbstractController
{
    /**
     * @param CommentRepository $commentRepository
     * @return Response
     */

   #[Route('admin/comment', name: 'app_admin_comment')]
   #[IsGranted('ROLE_ADMIN')]
   public function comment(CommentRepository $commentRepository,): Response
   {
      $comment = $commentRepository->findAll();

      return $this->render('admin/comment/comment.html.twig', [
         'comments' => $comment,
      ]);

   }

   /**
    * @param EntityManagerInterface $manager
    * @param Comment $comment
    * @return Response
    */
   #[Route('/admin/comment/view/{id}', name: 'app_admin_comment_view')]
   #[IsGranted('ROLE_ADMIN')]
   public function commentView(EntityManagerInterface $manager, Comment $comment):Response
   {
      $comment->setIsPublished(!$comment->isIsPublished());
      $manager->flush();
       if ($comment->isIsPublished()){
           $label = 'success';
           $sneak = 'affiché';
       } else {
           $label = 'info';
           $sneak = 'caché';
       }
       $this->addFlash($label, 'Le commentaire de '.$comment->getUser()->getUsername().' sur le tableau "'.$comment->getPainting()->getTitle().'" a bien été '.$sneak);
      return $this->redirectToRoute('app_admin_comment');
   }

   /**
    * @param Comment $comment
    * @param EntityManagerInterface $manager
    * @return Response
    */
   #[Route('/admin/comment/delete/{id}', 'app_admin_comment_delete')]
   #[IsGranted('ROLE_SUPER_ADMIN')]
   public function commentDelete(Comment $comment, EntityManagerInterface $manager):Response
   {
      $manager->remove($comment);
      $manager->flush();
       $this->addFlash('success', 'Le commentaire de '.$comment->getUser()->getUsername().' sur le tableau "'.$comment->getPainting()->getTitle().'" a bien été supprimé !');
      return $this->redirectToRoute('app_admin_comment');
   }
}
