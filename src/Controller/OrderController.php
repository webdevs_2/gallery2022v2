<?php

namespace App\Controller;

use App\Entity\Invoice;
use App\Repository\InvoiceRepository;
use App\Service\PdfService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * @return Response
     */
    #[Route('/order')]
    public function order(): Response
    {
        return $this->redirectToRoute('app_profile',['username' => $this->getUser()->getUsername()]);
    }

    /**
     * @param Invoice $invoice
     * @return Response
     */
    #[Route('/order/detail/{id}', name: 'app_order_detail')]
    public function orderDetail(Invoice $invoice): Response
    {
        $total = 0;
        foreach ($invoice->getOrders() as $order) {
            $total += $order->getPainting()->getPrice();
        }
        return $this->render('order/detail.html.twig', [
            'invoices' => $invoice,
            'total' => $total
        ]);
    }

    /**
     * @param Invoice $invoice
     * @param PdfService $pdfService
     * @return Response
     */
    #[Route('/order/pdf/{id}', name: 'app_order_pdf')]
    public function orderPdf(Invoice $invoice, PdfService $pdfService)
    {
        $total = 0;
        $logo = $this->imageToBase64($this->getParameter('kernel.project_dir'). '/public/img/icon/icon.png');
        foreach ($invoice->getOrders() as $order) {
            $total += $order->getPainting()->getPrice();
            $order->getPainting()->setImageName($this->imageToBase64($this->getParameter('kernel.project_dir'). '/public/img/tableau/'.$order->getPainting()->getImageName()));
        }
        $html = $this->renderView('order/pdf.html.twig', [
            'invoices' => $invoice,
            'total' => $total,
            'logo' => $logo
        ]);

        $name = $invoice->getUser()->getUsername().'-commande-numéro-'.$invoice->getId();
        $pdfService->showPdfFile($html,$name);

        return new Response(
            '',200,['Content-Type' => 'application/pdf']
        );
    }

    /**
     * @param $path
     * @return string
     */
    private function imageToBase64($path) {
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }

}
