<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Like;
use App\Entity\Painting;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use App\Repository\LikeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class DetailController extends AbstractController
{
    /**
     * @param Painting $painting
     * @param CommentRepository $commentRepository
     * @param LikeRepository $likeRepository
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param PaginatorInterface $paginator
     * @return Response
     */

    #[Route('/detail/{slug}', name: 'detail')]
    public function index(Painting $painting, CommentRepository $commentRepository, LikeRepository $likeRepository, Request $request, EntityManagerInterface $manager, PaginatorInterface $paginator): Response
    {
        $comments = $commentRepository->findBy(
            ['painting' => $painting, 'isPublished' => true],
            ['createdAt' => 'DESC']
        );

        $pagination = $paginator->paginate(
            $comments,
            $request->query->getInt('page', 1),
            3
        );

        $likes = $likeRepository->findBy(
            ['painting' => $painting, 'isLike' => true]
        );

        $likeUser = $likeRepository->findBy(
            ['painting' => $painting, 'isLike' => true, 'user' => $this->getUser()]
        );


        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $comment->setUser($this->getUser())
                ->setCreatedAt(new \DateTimeImmutable())
                ->setPainting($painting)
                ->setIsPublished(true);
            $manager->persist($comment);
            $manager->flush();
            $this->addFlash('success', 'Commentaire ajouté avec succès');
            return $this->redirectToRoute('detail', ['slug' => $painting->getSlug()]);
        }


        return $this->render('detail/detail.html.twig', [
            'painting' => $painting,
            'likes' => $likes,
            'comments' => $pagination,
            'likeUser' => $likeUser,
            'commentForm' => $form->createView()
        ]);
    }

    /**
     * @param Painting $painting
     * @param LikeRepository $likeRepository
     * @param EntityManagerInterface $manager
     * @return Response
     */

    #[Route('/detail/like/{slug}', name: 'like')]
    public function like(Painting $painting, LikeRepository $likeRepository, EntityManagerInterface $manager): Response
    {
        $like = new Like();
        $like->setIsLike('true')
            ->setUser($this->getUser())
            ->setPainting($painting)
            ->setCreatedAt(new \DateTimeImmutable());
        $manager->persist($like);
        $manager->flush();
        return $this->redirectToRoute('detail', ['slug' => $painting->getSlug()]);
    }

    /**
     * @param Like $like
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/detail/unlike/{slug}/{id}', name: 'unlike')]
    public function unLike(Like $like, EntityManagerInterface $manager): Response
    {
        $slug = $like->getPainting()->getSlug();
        $manager->remove($like);
        $manager->flush();
        return $this->redirectToRoute('detail', ['slug' => $slug]);
    }

    /**
     * @param Painting $painting
     * @param LikeRepository $likeRepository
     * @param SessionInterface $session
     * @return Response
     */

    #[Route('/detail/{slug}/likes', name: 'likes')]
    public function likes(Painting $painting, LikeRepository $likeRepository, SessionInterface $session): Response
    {

        if (!$this->getUser()) {
            return $this->redirectToRoute('detail', ['slug' => $painting->getSlug()]);
        }

        $likes = $likeRepository->findBy(
            ['painting' => $painting, 'isLike' => true]
        );



        return $this->render('detail/likes.html.twig', [
            'likes' => $likes,
            'session' => $session->get('panier')
        ]);
    }
}
