<?php

namespace App\Controller;

use App\Repository\SliderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class SliderController extends AbstractController
{
    /**
     * @param SliderRepository $repository
     * @return Response
     */
    public function slider(SliderRepository $repository): Response
    {
        $sliders = $repository->findBy(['IsVisible'=>true]);
        return $this->render('partials/slider.html.twig',[
            'sliders' => $sliders
        ]);
    }
}
