<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditProfileType;
use App\Repository\LikeRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{

    /**
     * @return Response
     */
    #[Route('/profile')]
    public function index(): Response
    {
        return $this->redirectToRoute('app_profile',['username' => $this->getUser()->getUsername()]);
    }

    /**
     * @param User $user
     * @return Response
     */
    #[Route('/profile/{username}', name: 'app_profile')]
    public function profile(User $user): Response
    {
        return $this->render('profile/profile.html.twig',[
            'specUser' => $user
        ]);
    }

    /**
     * @param User $user
     * @param LikeRepository $likeRepository
     * @return Response
     */

    #[Route('/profile/{username}/likes', name: 'app_profile_likes')]
    public function profileLikes(User $user,LikeRepository $likeRepository): Response
    {
        $likes = $likeRepository->findBy(
            ['user' => $user]
        );

        return $this->render('profile/profileLikes.html.twig',[
            'likes' => $likes,
            'user' => $user
        ]);
    }

    /**
     * @param User $user
     * @param UserRepository $userRepository
     * @return Response
     */

    #[Route('/profile/{username}/edit', name: 'app_profile_edit')]
    public function profileEdit(User $user, UserRepository $userRepository, Request $request, EntityManagerInterface $manager ): Response
    {
        $form = $this->createForm(EditProfileType::class,$user);
        // Verify if wrong user edit or not super admin
        if ($this->getUser()->getUserIdentifier() != $user->getUserIdentifier() && !$this->isGranted('ROLE_ADMIN')) {
            $users = $userRepository->findBy(
                ['email' => $this->getUser()->getUserIdentifier()]
            );
            return $this->redirectToRoute('app_profile',['username' => $users[0]->getUsername()]);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setUpdatedAt(new \DateTimeImmutable());
            $manager->persist($user);
            $manager->flush();
            $this->addFlash('success','Changement effectué avec succès');
            return $this->redirectToRoute('app_profile',['username' => $user->getUsername()]);
        }
        return $this->render('profile/profileEdit.html.twig',[
            'editForm' => $form->createView()
        ]);
    }
}
