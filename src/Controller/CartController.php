<?php

namespace App\Controller;

use App\Entity\Invoice;
use App\Entity\Message;
use App\Entity\Order;
use App\Entity\Painting;
use App\Repository\InvoiceRepository;
use App\Repository\MessageRepository;
use App\Repository\PaintingRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

class CartController extends AbstractController
{
    /**
     * @param SessionInterface $session
     * @param PaintingRepository $paintingRepository
     * @return Response
     */
    #[Route('/panier', name: 'app_cart')]
    public function index(SessionInterface $session,PaintingRepository $paintingRepository): Response
    {
        $panier = $session->get('panier', []);
        $panierData = [];
        $total = 0;

        if (!empty($panier)) {
            foreach ($panier AS $id => $quantity) {
                $panierData[] = [
                    'painting' => $paintingRepository->find($id),
                    'quantity' => 1
                ];
                $total += $paintingRepository->find($id)->getPrice();
            }
        }

        return $this->render('cart/cart.html.twig', [
            'carts' => $panierData,
            'total' => $total
        ]);
    }

    /**
     * @param Painting $painting
     * @param SessionInterface $session
     * @param Request $request
     * @param RouterInterface $router
     * @return RedirectResponse
     */
    #[Route('/panier/add/{slug}', name: 'app_cart_add')]
    public function cartAdd(Painting $painting, SessionInterface $session, Request $request, RouterInterface $router)
    {
        $r = $router->match(Request::create($request->headers->get('referer'))->getPathInfo());

        $panier = $session->get('panier', []);

        foreach ($panier as $key => $value) {
            if ($painting->getId() == $key) {
                $this->addFlash('warning','Le tableau "'.$painting->getTitle().'" est déjà dans votre panier');
                return match ($r['_route']) {
                    'app_profile_likes' => $this->redirectToRoute($r['_route'],['username' => $this->getUser()->getUsername()]),
                    'detail' => $this->redirectToRoute($r['_route'],['slug' => $r['slug']]),
                    default => $this->redirectToRoute('app_cart'),
                };
            }
        }
        $panier[$painting->getId()] = 1;

        $session->set('panier',$panier);
        $this->addFlash('success','Le tableau "'.$painting->getTitle().'" a été ajouté dans votre panier');
        return match ($r['_route']) {
            'app_profile_likes' => $this->redirectToRoute($r['_route'],['username' => $this->getUser()->getUsername()]),
            'detail' => $this->redirectToRoute($r['_route'],['slug' => $r['slug']]),
            default => $this->redirectToRoute('app_cart'),
        };

    }

    /**
     * @param Painting $painting
     * @param SessionInterface $session
     * @param Request $request
     * @param RouterInterface $router
     * @return Response
     */
    #[Route('panier/remove/{id}', name: 'app_cart_delete')]
    public function cartDelete(Painting $painting,SessionInterface $session,Request $request, RouterInterface $router): Response
    {
        $panier = $session->get('panier',[]);
        $r = $router->match(Request::create($request->headers->get('referer'))->getPathInfo());
        if (!empty($panier[$painting->getId()])) {
            unset($panier[$painting->getId()]);
        }
        $session->set('panier',$panier);
        $this->addFlash('success','Le tableau "'.$painting->getTitle().'" a bien été retiré du panier');
        return match ($r['_route']) {
            'app_profile_likes' => $this->redirectToRoute($r['_route'],['username' => $this->getUser()->getUsername()]),
            'detail' => $this->redirectToRoute($r['_route'],['slug' => $r['slug']]),
            default => $this->redirectToRoute('app_cart'),
        };
    }

    /**
     * @param InvoiceRepository $invoiceRepository
     * @param EntityManagerInterface $manager
     * @param PaintingRepository $paintingRepository
     * @param SessionInterface $session
     * @param UserRepository $userRepository
     * @return Response
     */
    #[Route('panier/buy', name: 'app_cart_buy')]
    public function cartBuy(InvoiceRepository $invoiceRepository, EntityManagerInterface $manager, PaintingRepository $paintingRepository,SessionInterface $session, UserRepository $userRepository): Response
    {
        $panier = $session->get('panier', []);

        $admin = $userRepository->findBy(
            ['email' => 'admin.gallery@gallery.com']
        );

        if (empty($panier)) {
            $this->addFlash('danger', 'Votre panier est vide');
            return $this->redirectToRoute('app_cart');
        }

        $invoice = new Invoice();
        $invoice->setUser($this->getUser())
                ->setCreatedAt(new \DateTimeImmutable());
        $manager->persist($invoice);
        $manager->flush();

        $lastInvoices = $invoiceRepository->findBy(
            [],
            ['id' => 'DESC'],
            1
        );

        foreach ($panier as $painting => $quantity) {
            $order = new Order();
            $order->setPainting($paintingRepository->find($painting))
                  ->setQuantity($quantity)
                 ->setInvoice($lastInvoices[0]);
            $manager->persist($order);
        }

        $message = new Message();
        $message->setSender($admin[0])
                ->setRecipient($this->getUser())
                ->setCreatedAt(new \DateTimeImmutable())
                ->setTitle('Commande Numéro '. $invoice->getId())
                ->setMessage('
                <h3>Bonjour '.$this->getUser()->getUsername().',</h3>
                <p>Nous vous confirmons votre commande numéro '.$invoice->getId().'.</p>
                <p>Vous pouvez accéder à votre facture via votre <a href="https://127.0.0.1:8000/profile/'.$this->getUser()->getUsername().'">profile</a> ou la télécharger <a href="https://127.0.0.1:8000/order/pdf/'.$invoice->getId().'">ici</a>.</p
                <p>Nous vous souhaitons bonne visite sur notre site et remercions pour votre achat !</p>
                <p>Cordialement</p>
                <p>L\'équipe Gallery</p>
                ');
        $manager->persist($message);

        $manager->flush();
        $session->remove('panier');
        return $this->redirectToRoute('app_order_detail', ['id' => $lastInvoices[0]->getId()]);
    }
}
