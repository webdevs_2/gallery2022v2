<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

class MessagerieController extends AbstractController
{

    /**
     * @param MessageRepository $messageRepository
     * @return Response
     */
    #[Route('/messages', name: 'app_message')]
    public function index(MessageRepository $messageRepository): Response
    {
        $messagesR = $messageRepository->findBy(
            ['recipient' => $this->getUser(),'isVisible' => true],

        );

        $messagesE = $messageRepository->findBy(
            ['sender' => $this->getUser(),'isVisible' => true],

        );
        return $this->render('messagerie/index.html.twig',[
            'messagesR' => $messagesR,
            'messagesE' => $messagesE
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/message/send', name: 'app_message_send')]
    public function send(Request $request, EntityManagerInterface $manager): Response
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $message->setSender($this->getUser());

            $manager->persist($message);
            $manager->flush();

            $this->addFlash('success', 'Votre message a été envoyé avec succès.');
            return $this->redirectToRoute('app_message_sent');
        }

        return $this->render('messagerie/send.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param MessageRepository $repository
     * @return Response
     */
    #[Route('/message/received', name: 'app_message_received')]
    public function received(MessageRepository $repository): Response
    {
        $messages = $repository->findBy(
            ['isVisible' => true, 'recipient' => $this->getUser()],
            ['id' => 'DESC']
        );
        return $this->render('messagerie/received.html.twig', [
            'messages' => $messages
        ]);
    }

    /**
     * @param MessageRepository $messageRepository
     * @return Response
     */
    #[Route('/message/sent', name: 'app_message_sent')]
    public function sent(MessageRepository $messageRepository): Response
    {
        $messages = $messageRepository->findBy(
            ['sender' => $this->getUser()],
            ['id' => 'DESC']
        );
        return $this->render('messagerie/sent.html.twig', [
            'messages' => $messages
        ]);
    }

    /**
     * @param Message $message
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/message/read/{id}', name: 'app_message_read')]
    public function read(Message $message, EntityManagerInterface $manager): Response
    {
        if ($message->getSender() !== $this->getUser()) {
            $message->setIsRead(true)
                    ->setReadedAt(new \DateTimeImmutable());
            $manager->persist($message);
            $manager->flush();
        }

        return $this->render('messagerie/read.html.twig', compact("message"));
    }

    /**
     * @param Message $message
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/message/delete/{id}', name: 'app_message_delete')]
    public function delete(Message $message,EntityManagerInterface $manager, Request $request, RouterInterface $router): Response
    {

        $r = $router->match(Request::create($request->headers->get('referer'))->getPathInfo());


        if ($message->getSender() === $this->getUser()) {
            $manager->remove($message);
        } else {
            $message->setIsVisible(false);
            $manager->persist($message);
        }
        $manager->flush();
        $this->addFlash('success', 'Message supprimé');

        return match ($r['_route']) {
            'app_message_sent' => $this->redirectToRoute($r['_route']),
            default => $this->redirectToRoute('app_message_received'),
        };
    }
}
