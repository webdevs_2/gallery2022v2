<?php

namespace App\DataFixtures;

use App\Entity\Like;
use App\Entity\Painting;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class LikeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $users = $manager->getRepository(User::class)->findAll();
        $paintings = $manager->getRepository(Painting::class)->findAll();
        $countPaintings = count($paintings);
        $faker = Factory::create();
        foreach ( $users as $user) {
            $like = new Like();
            $like->setIsLike(true)
                 ->setCreatedAt(new \DateTimeImmutable())
                 ->setUser($user)
                 ->setPainting($paintings[$faker->numberBetween(0,$countPaintings - 1)]);
            $manager->persist($like);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
