<?php

namespace App\DataFixtures;

use App\Entity\Message;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class MessageFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $users = $manager->getRepository(User::class)->findAll();
        $countU = count($users);
        $faker = Factory::create();

        for ($i=1;$i<=200;$i++) {
            $message = new Message();
            $message->setTitle($faker->words(3,true))
                    ->setSender($users[$faker->numberBetween(0,$countU - 1)])
                    ->setRecipient($users[$faker->numberBetween(0,$countU - 1)])
                    ->setMessage($faker->paragraphs(2,true));
            $manager->persist($message);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }


}
