<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Painting;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $paintings = $manager->getRepository(Painting::class)->findAll();
        $users = $manager->getRepository(User::class)->findAll();
        $countPaints = count($paintings);
        $countUsers = count($users);
        $faker = Factory::create();
        for ($i = 1; $i <= 300; $i++) {
            $comment = new Comment();
            $comment->setUser($users[$faker->numberBetween(0,$countUsers - 1)])
                    ->setPainting($paintings[$faker->numberBetween(0,$countPaints - 1)])
                    ->setComment($faker->paragraph(2,true))
                    ->setCreatedAt(new \DateTimeImmutable())
                    ->setIsPublished($faker->boolean(60));
            $manager->persist($comment);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            PaintingFixtures::class,
            UserFixtures::class
        ];
    }
}
