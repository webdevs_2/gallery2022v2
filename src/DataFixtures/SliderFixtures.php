<?php

namespace App\DataFixtures;

use App\Entity\Slider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SliderFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i=1;$i<=5;$i++) {
            $slider = new Slider();
            $slider->setImageName('slider'.$i.'.jpg')
                   ->setIsVisible(true)
                   ->setUpdatedAt(new \DateTimeImmutable());
            $manager->persist($slider);
        }

        $manager->flush();
    }
}
