<?php

namespace App\DataFixtures;

use App\Entity\Invoice;
use App\Entity\Order;
use App\Entity\Painting;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class OrderFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $paintings = $manager->getRepository(Painting::class)->findAll();
        $invoices = $manager->getRepository(Invoice::class)->findAll();
        $countP = count($paintings);
        $faker = Factory::create();

        foreach ($invoices as $invoice) {
            $order = new Order();
            $order->setPainting($paintings[$faker->numberBetween(0,$countP - 1)])
                  ->setInvoice($invoice)
                  ->setQuantity(1);
            $manager->persist($order);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            InvoiceFixtures::class
        ];
    }
}
