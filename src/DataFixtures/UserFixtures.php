<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use Cocur\Slugify\Slugify;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private object $hasher;
    private array $genders = ['male', 'female'];
    public function __construct(UserPasswordHasherInterface $hasher) {
        $this->hasher = $hasher;
    }
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $slug = new Slugify();
        for($i = 1; $i <= 100; $i++) {
            $user = new User();
            $gender = $faker->randomElement($this->genders);
            $firstName = $faker->firstName($gender);
            $lastname = $faker->lastName;
            $user   ->setFirstName($firstName)
                ->setLastName($lastname)
                ->setUsername($firstName.'.'.$lastname.$faker->numberBetween(1,99))
                ->setDescription($faker->paragraph(6,))
                ->setEmail($slug->slugify($user->getFirstName()) . '.' . $slug->slugify($user->getLastName()) . '@gmail.com')
                ->setImageName('default.jpg')
                ->setPassword($this->hasher->hashPassword($user, 'password'))
                ->setCreatedAt(new \DateTimeImmutable())
                ->setUpdatedAt(new \DateTimeImmutable())
                ->setIsDisabled(false)
                ->setRoles(['ROLE_USER']);
            $manager->persist($user);
        }

        $user = new User();
        $user   ->setFirstName('Pat')
            ->setLastName('Mar')
            ->setUsername('patmar')
            ->setDescription('Je suis un fin connaisseur en mousse de bière !')
            ->setEmail('pat.mar@gmail.com')
            ->setImageName('074m.jpg')
            ->setPassword($this->hasher->hashPassword($user, 'password'))
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setIsDisabled(false)
            ->setRoles(['ROLE_SUPER_ADMIN']);
        $manager->persist($user);

        $user = new User();
        $user   ->setFirstName('admin')
            ->setLastName('gallery')
            ->setUsername('admin')
            ->setDescription('Adresse mail administrateur de Gallery')
            ->setEmail('admin.gallery@gallery.com')
            ->setImageName('default.jpg')
            ->setPassword($this->hasher->hashPassword($user, 'admin'))
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setIsDisabled(false)
            ->setRoles(['ROLE_SUPER_ADMIN']);
        $manager->persist($user);
        $manager->flush();
    }
}
