<?php

namespace App\DataFixtures;

use App\Entity\Invoice;
use App\Entity\Order;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class InvoiceFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $orders = $manager->getRepository(Order::class)->findAll();
        $users = $manager->getRepository(User::class)->findAll();
        $faker = Factory::create();
        $countO = count($orders);
        $countU = count($users);
        for ($i=1;$i<=50;$i++) {
            $invoice = new Invoice();
            $invoice->setUser($users[$faker->numberBetween(0,$countU - 1)])
                    ->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($invoice);
        }

        $manager->flush();
    }
}
