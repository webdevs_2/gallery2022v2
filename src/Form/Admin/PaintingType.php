<?php

namespace App\Form\Admin;

use App\Entity\Category;
use App\Entity\Painting;
use App\Entity\Technical;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PaintingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre du tableau',
            ])
            ->add('author', TextType::class, [
                'label' => 'Nom de l\'auteur',
            ])
            ->add('makedAt', DateType::class, [
                'label' => 'Date de création du tableau',
                'widget' => 'single_text',
                'input' => 'datetime_immutable',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description du tableau',
            ])
            ->add('height', IntegerType::class, [
                'label' => 'Hauteur du tableau',
            ])
            ->add('width', IntegerType::class, [
                'label' => 'Largeur du tableau',
            ])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image du tableau',
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
                'image_uri' => true
            ])
            ->add('isPublished', ChoiceType::class, [
                'label' => 'Afficher',
                'choices' => ['oui' => 1, 'non' => 0],
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                'placeholder' => 'Choisissez...'
            ])
            ->add('technical', EntityType::class, [
                'class' => Technical::class,
                'choice_label' => 'name',
                'placeholder' => 'Choisissez...'
            ])
            ->add('price',IntegerType::class, [
                'label' => 'Prix du tableau'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Painting::class,
        ]);
    }
}
