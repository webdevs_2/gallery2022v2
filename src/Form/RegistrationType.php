<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichImageType;
use VictorPrdh\RecaptchaBundle\Form\ReCaptchaType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName',TextType::class,[
                'label' => 'Votre prénom',
                'attr' => ['placeholder' => 'Bruce']
            ])
            ->add('lastName',TextType::class, [
                'label' => 'Votre nom',
                'attr' => ['placeholder' => 'Wayne']
            ])
            ->add('username',TextType::class, [
                'label' => 'votre username',
                'attr' => ['placeholder' => 'Batman']
            ])
            ->add('email',EmailType::class, [
                'label' => 'Votre émail',
                'attr' => ['placeholder' => 'bruce.wayne@batman.com']
            ])
            ->add('pwd', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Le mot de passe ne correspond pas.',
                'options' => ['attr' => ['class' => 'password-field']],
                'mapped' => false,
                'first_options'  => ['label' => 'Votre mot de passe '],
                'second_options' => ['label' => 'Répéter votre mot de passe'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrez un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit contenir au minimum {{ limit }} caractères',
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('imageFile',VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_uri' => false,
                'image_uri' => false
            ])
            ->add("recaptcha", ReCaptchaType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
